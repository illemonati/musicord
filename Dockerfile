FROM rust:1.59.0 as build

RUN USER=root cargo new --bin musicord
WORKDIR /musicord

COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

RUN cargo build --release
RUN rm src/*.rs

COPY ./src ./src


RUN rm ./target/release/deps/musicord*
RUN cargo install --path .

FROM debian:bullseye-slim as release

WORKDIR /musicord

RUN apt update
RUN apt install libopus-dev -y
RUN apt install ffmpeg -y
RUN apt install youtube-dl -y

COPY --from=build /musicord/target/release/musicord .

CMD ["./musicord"]