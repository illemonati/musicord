mod ping;

use ping::PING_COMMAND;
use serenity::framework::standard::macros::group;

#[group]
#[summary = "Commands for testing purposes"]
#[commands(ping)]
struct Testing;
