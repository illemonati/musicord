pub mod join;
pub mod pause;
pub mod play;
pub mod resume;
pub mod skip;
pub mod stop;

use join::JOIN_COMMAND;
use pause::PAUSE_COMMAND;
use play::PLAY_COMMAND;
use resume::RESUME_COMMAND;
use serenity::framework::standard::macros::group;
use skip::SKIP_COMMAND;
use stop::STOP_COMMAND;

#[group]
#[commands(join, play, pause, resume, skip, stop)]
#[summary = "Commands for controlling the music player"]
pub struct Player;
