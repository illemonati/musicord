use serenity::{
    client::Context,
    framework::standard::{macros::command, CommandError, CommandResult},
    model::channel::Message,
};

#[command]
#[only_in(guilds)]
#[description("Skips the currently top track")]
pub async fn skip(ctx: &Context, msg: &Message) -> CommandResult {
    let guild = match msg.guild(&ctx.cache).await {
        Some(g) => g,
        None => return Err(CommandError::from("Guild not found in cache")),
    };

    let sb_manager = match songbird::get(&ctx).await {
        Some(m) => m.clone(),
        None => return Err(CommandError::from("Song Bird manager not found")),
    };

    match sb_manager.get(guild.id) {
        Some(handler_mux) => {
            let handler = handler_mux.lock().await;
            match handler.queue().skip() {
                Ok(_) => {}
                Err(e) => {
                    msg.reply(&ctx.http, format!("Could Not Skip Track: {:?}", e))
                        .await?;
                }
            }
        }
        None => {
            msg.reply(
                &ctx.http,
                format!("Must Join Voice Channel First Before Skip"),
            )
            .await?;
        }
    }

    Ok(())
}
