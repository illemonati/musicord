use serenity::{
    client::Context,
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
};

use crate::player::join::join;
use crate::queue::enqueue::enqueue;

#[command]
#[only_in(guilds)]
#[description("Joins a Voice Channel and Enqueues a track")]
pub async fn play(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    join(ctx, msg, args.clone()).await?;
    enqueue(ctx, msg, args).await?;

    Ok(())
}
