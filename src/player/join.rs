use serenity::client::Context;
use serenity::framework::standard::macros::command;
use serenity::framework::standard::{Args, CommandError, CommandResult};
use serenity::model::channel::Message;
use serenity::prelude::Mentionable;

#[command]
#[only_in(guilds)]
#[description("Join the voice channel sender is present in")]
pub async fn join(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let no_message = match args.single::<String>() {
        Ok(msg) => msg == "no-message",
        Err(_) => false,
    };

    let guild = match msg.guild(&ctx.cache).await {
        Some(g) => g,
        None => return Err(CommandError::from("Guild not found in cache")),
    };

    let vc_id = guild
        .voice_states
        .get(&msg.author.id)
        .and_then(|vs| vs.channel_id);

    let vs_id = match vc_id {
        Some(id) => id,
        None => {
            msg.reply(&ctx.http, "Please Join A Voice Channel").await?;
            return Ok(());
        }
    };

    let sb_manager = match songbird::get(&ctx).await {
        Some(m) => m.clone(),
        None => return Err(CommandError::from("Song Bird manager not found")),
    };

    if let Some(handle_mux) = sb_manager.get(guild.id) {
        if let Some(channel) = handle_mux.lock().await.current_channel() {
            if channel.0 == vs_id.0 {
                return Ok(());
            }
        }
    }
    let (handle_mux, success) = sb_manager.join(guild.id, vs_id).await;
    match success {
        Ok(_) => {
            if !no_message {
                msg.reply(
                    &ctx.http,
                    format!("Joined Voice Channel {}", vs_id.mention()),
                )
                .await?;
            }
            let mut handler = handle_mux.lock().await;
            handler.deafen(true).await?;
        }
        Err(e) => {
            msg.reply(
                &ctx.http,
                format!(
                    "Unable to Join Voice Channel {} Due to {}",
                    vs_id.mention(),
                    e
                ),
            )
            .await?;
        }
    }

    Ok(())
}
