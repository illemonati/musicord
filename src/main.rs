mod constants;
mod player;
mod queue;
mod testing;
mod utils;

use constants::PREFIX;
use player::PLAYER_GROUP;
use queue::QUEUE_GROUP;
use serenity::async_trait;
use serenity::client::{Client, Context, EventHandler};
use serenity::framework::standard::macros::help;
use serenity::framework::standard::{help_commands::with_embeds, StandardFramework};
use serenity::framework::standard::{Args, CommandGroup, CommandResult, HelpOptions};
use serenity::model::channel::Message;
use serenity::model::id::UserId;
use serenity::model::prelude::{Activity, Ready};
use songbird::SerenityInit;
use std::collections::HashSet;
use std::env;
use testing::TESTING_GROUP;
use tokio::signal;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, context: Context, ready: Ready) {
        println!("Musicord Online! Logged in as {}", ready.user.tag());
        context
            .set_activity(Activity::listening(format!("{}help for options", PREFIX)))
            .await;
    }
}

#[help]
async fn help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    let _ = with_embeds(context, msg, args, &help_options, groups, owners).await;
    Ok(())
}

#[tokio::main]
async fn main() {
    let framework = StandardFramework::new()
        .configure(|c| c.prefix(PREFIX))
        .help(&HELP)
        .group(&TESTING_GROUP)
        .group(&PLAYER_GROUP)
        .group(&QUEUE_GROUP);

    let token = env::var("MUSICORD_DISCORD_TOKEN").expect("token");
    let mut client = Client::builder(token)
        .event_handler(Handler)
        .framework(framework)
        .register_songbird()
        .await
        .expect("Error creating client");

    if let Err(e) = client.start().await {
        println!("An error occurred while running the client: {:?}", e);
    }
    tokio::spawn(async move {
        let _ = client
            .start()
            .await
            .map_err(|e| println!("An error occurred while running the client: : {:?}", e));
    });
    let _ = signal::ctrl_c().await;
    println!("Ctrl-C received, exiting.")
}
