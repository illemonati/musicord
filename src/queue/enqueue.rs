use std::time::Duration;

use crate::{
    constants::PREFIX,
    queue::event_handlers::{TrackEndHandler, TrackPauseHandler, TrackPlayHandler},
    utils::fmt_duration,
};
use serenity::{
    client::Context,
    framework::standard::{macros::command, Args, CommandError, CommandResult},
    model::channel::Message,
};
use songbird::{
    input::{Input, Restartable},
    tracks, Event, TrackEvent,
};

#[command]
#[description("Enqueues a track into the queue from a url")]
#[usage("<url>")]
#[only_in(guilds)]
pub async fn enqueue(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let url = match args.single::<String>() {
        Ok(url) => url,
        Err(_) => {
            msg.reply(
                &ctx.http,
                format!(
                    "Please input one argument for track url!\nie. {}enqueue <url>",
                    PREFIX
                ),
            )
            .await?;
            return Ok(());
        }
    };

    let guild = match msg.guild(&ctx.cache).await {
        Some(g) => g,
        None => return Err(CommandError::from("Guild not found in cache")),
    };

    let sb_manager = match songbird::get(&ctx).await {
        Some(m) => m.clone(),
        None => return Err(CommandError::from("Song Bird manager not found")),
    };

    match sb_manager.get(guild.id) {
        Some(handler_mux) => {
            let mut handler = handler_mux.lock().await;
            let source = match Restartable::ytdl(url.clone(), true).await {
                Ok(source) => source,
                Err(e) => {
                    msg.reply(
                        &ctx.http,
                        format!("Unable to start playback of {}: {:?}", &url, e),
                    )
                    .await?;
                    return Ok(());
                }
            };

            let input: Input = source.into();

            let meta = input.metadata.clone();

            let (track, track_handle) = tracks::create_player(input);

            track_handle.add_event(
                Event::Track(TrackEvent::Play),
                TrackPlayHandler {
                    chan_id: msg.channel_id,
                    http: ctx.http.clone(),
                },
            )?;

            track_handle.add_event(
                Event::Track(TrackEvent::End),
                TrackEndHandler {
                    chan_id: msg.channel_id,
                    http: ctx.http.clone(),
                    handler_mux: handler_mux.clone(),
                },
            )?;

            track_handle.add_event(
                Event::Track(TrackEvent::Pause),
                TrackPauseHandler {
                    chan_id: msg.channel_id,
                    http: ctx.http.clone(),
                },
            )?;

            handler.enqueue(track);

            msg.reply(
                &ctx.http,
                format!(
                    "Added \"{}\" from {} ({}) to queue at index {} (index starts at 1)",
                    meta.title.unwrap_or("Untitled".into()),
                    meta.artist.unwrap_or("Anonymous".into()),
                    fmt_duration(&meta.duration.unwrap_or(Duration::from_secs(0))),
                    handler.queue().len()
                ),
            )
            .await?;
        }
        None => {
            msg.reply(
                &ctx.http,
                format!("Must Join Voice Channel First Before Enqueue"),
            )
            .await?;
        }
    }

    Ok(())
}
