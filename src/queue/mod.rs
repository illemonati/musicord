pub mod enqueue;
pub mod event_handlers;

use enqueue::ENQUEUE_COMMAND;
use serenity::framework::standard::macros::group;

#[group]
#[summary = "Commands for controlling the music queue"]
#[commands(enqueue)]
pub struct Queue;
