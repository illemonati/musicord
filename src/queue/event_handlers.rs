use std::{sync::Arc, time::Duration};

use serenity::{async_trait, http::Http, model::prelude::ChannelId, prelude::Mutex};
use songbird::{Call, Event, EventContext, EventHandler};

use crate::utils::fmt_duration;

pub struct TrackPlayHandler {
    pub chan_id: ChannelId,
    pub http: Arc<Http>,
}

#[async_trait]
impl EventHandler for TrackPlayHandler {
    async fn act(&self, ctx: &EventContext<'_>) -> Option<Event> {
        if let EventContext::Track(track_list) = ctx {
            let (_, track_handle) = track_list.get(0).expect("Did not have track");
            let meta = track_handle.metadata();
            let info = track_handle.get_info().await.expect("Did not have info");
            let _ = self
                .chan_id
                .say(
                    &self.http,
                    &format!(
                        "Now Playing  \"{}\" from {} ({}), Starting from ({})",
                        meta.title.as_ref().unwrap_or(&"Untitled".to_string()),
                        meta.artist.as_ref().unwrap_or(&"Anonymous".to_string()),
                        fmt_duration(&meta.duration.unwrap_or(Duration::from_secs(0))),
                        fmt_duration(&info.position)
                    ),
                )
                .await;
        }
        None
    }
}

pub struct TrackPauseHandler {
    pub chan_id: ChannelId,
    pub http: Arc<Http>,
}

#[async_trait]
impl EventHandler for TrackPauseHandler {
    async fn act(&self, ctx: &EventContext<'_>) -> Option<Event> {
        if let EventContext::Track(track_list) = ctx {
            let (_, track_handle) = track_list.get(0).expect("Did not have track");
            let meta = track_handle.metadata();
            let info = track_handle.get_info().await.expect("Did not have info");
            let _ = self
                .chan_id
                .say(
                    &self.http,
                    &format!(
                        "Now Paused  \"{}\" from {} ({}) at from ({})",
                        meta.title.as_ref().unwrap_or(&"Untitled".to_string()),
                        meta.artist.as_ref().unwrap_or(&"Anonymous".to_string()),
                        fmt_duration(&meta.duration.unwrap_or(Duration::from_secs(0))),
                        fmt_duration(&info.position)
                    ),
                )
                .await;
        }
        None
    }
}

pub struct TrackEndHandler {
    pub chan_id: ChannelId,
    pub http: Arc<Http>,
    pub handler_mux: Arc<Mutex<Call>>,
}

#[async_trait]
impl EventHandler for TrackEndHandler {
    async fn act(&self, ctx: &EventContext<'_>) -> Option<Event> {
        if let EventContext::Track(_) = ctx {
            let handler = self.handler_mux.lock().await;
            match handler.queue().current() {
                Some(_next_track) => {
                    // let meta = next_track.metadata();
                    // let _ = self
                    //     .chan_id
                    //     .say(
                    //         &self.http,
                    //         &format!(
                    //             "Now Playing  \"{}\" from {} ({})",
                    //             meta.title.as_ref().unwrap_or(&"Untitled".to_string()),
                    //             meta.artist.as_ref().unwrap_or(&"Anonymous".to_string()),
                    //             fmt_duration(&meta.duration.unwrap_or(Duration::from_secs(0))),
                    //         ),
                    //     )
                    //     .await;
                }
                None => {
                    let _ = self
                        .chan_id
                        .say(&self.http, "Finished Playing All Tracks")
                        .await;
                }
            }
        }

        None
    }
}
